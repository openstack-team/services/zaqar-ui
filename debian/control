Source: zaqar-ui
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 openstack-dashboard,
 python3-babel,
 python3-coverage,
 python3-django,
 python3-django-babel,
 python3-hacking,
 python3-openstackdocstheme,
 python3-sphinxcontrib.apidoc,
 python3-testtools,
 python3-zaqarclient,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/openstack-team/horizon-plugins/zaqar-ui
Vcs-Git: https://salsa.debian.org/openstack-team/horizon-plugins/zaqar-ui.git
Homepage: https://github.com/openstack/zaqar-ui

Package: python3-zaqar-ui
Architecture: all
Depends:
 openstack-dashboard,
 python3-babel,
 python3-django,
 python3-django-babel,
 python3-pbr,
 python3-zaqarclient,
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack Queueing as a Service - Dashboard plugin
 Zaqar is a multi-tenant cloud messaging service for web developers. It
 combines the ideas pioneered by Amazon's SQS product with additional semantics
 to support event broadcasting.
 .
 The service features a fully RESTful API, which developers can use to send
 messages between various components of their SaaS and mobile applications, by
 using a variety of communication patterns. Underlying this API is an efficient
 messaging engine designed with scalability and security in mind.
 .
 Other OpenStack components can integrate with Zaqar to surface events to end
 users and to communicate with guest agents that run in the "over-cloud" layer.
 Cloud operators can leverage Zaqar to provide equivalents of SQS and SNS to
 their customers.
 .
 This package contains the OpenStack dashboard plugin.
